---
title: Équipe et partenaires
description: 
hide:
- navigation
---

## Qui sommes nous ?

Nous sommes une [équipe](https://tinyl.io/41Ii) de professeur·e·s d'informatique, d'enseignant·e·s chercheur·e·s dans ce domaine et d'ingénieur·e pédagogique qui nous rassemblons pour identifier les besoins, rassembler ou créer les ressources qui vont pouvoir aider les collègues et futurs collègues.

Nous travaillons sous forme d'une équipe projet et co-animons forum et communauté. Vous allez nous croiser au fil des discussions et des ressources.

## Les partenaires et soutiens

Le projet est porté et opéré par le [Learning Lab Inria](https://learninglab.inria.fr) avec la [Communauté d'Apprentissage de l'Informatique](https://cai.community) en appui :

| [![Learning Lab Inria](./assets/logo-learninglab-inria.png)](https://learninglab.inria.fr) | [![Communauté d'Apprentissage de l'Informatique](https://medsci-sites.inria.fr/cai/wp-content/uploads/sites/3/2020/04/cropped-logo-cai-0-4.png)](https://cai.community) |
|---|---|

en partenariat avec les établissements d'enseignement supérieurs et de recherche ou associatifs suivants :

| [![ENS Paris Saclay](./assets/logo-ens-saclay.png "ENS Paris Saclay")](https://ens-paris-saclay.fr) | [![Université Libre de Bruxelles](https://cai.community/wp-content/uploads/sites/3/2020/12/logo-300x101.png "Université Libre de Bruxelles")](http://www.ulb.ac.be) | [![Université de la Réunion](https://cai.community/wp-content/uploads/sites/3/2020/06/logo_ur.jpg "Université de la Réunion")](https://www.univ-reunion.fr) | [![Université Côte d'Azur](https://cai.community/wp-content/uploads/sites/3/2020/06/logo_uca.jpg "Université Côte d'Azur")](https://univ-cotedazur.fr "") | [![Association des enseignantes et enseignants d'informatique de France](./assets/logo-aeif.png "Association des enseignantes et enseignants d'informatique de France")](http://aeif.fr)
|---|---|---|---|---|

et sous l'égide de la [Société Informatique de France](https://www.societe-informatique-de-france.fr) et de l'association [Class´Code](https://classcode.fr) et avec le soutien de la [Direction du Numérique pour l’Éducation](https://www.education.gouv.fr/direction-du-numerique-pour-l-education-dne-9983) et d'[Inria](https://www.inria.fr) :

| [![Société Informatique de France](./assets/logo-sif.png)](https://www.societe-informatique-de-france.fr) | [![Class´Code](https://project.inria.fr/classcode/files/2016/07/LOGO-ClassCode-coul.jpg)](https://classcode.fr) | [![Direction du Numérique pour l’Éducation](./assets/logo-menj.png)](https://www.education.gouv.fr/direction-du-numerique-pour-l-education-dne-9983) | [![Inria](https://cai.community/wp-content/uploads/sites/3/2020/05/inr_logo_rouge-300x106.png)](https://www.inria.fr) |
|---|---|---|---|

## Vos questions !

### Quels sont les documents de pilotage du projet ?

En toute transparence voici nos documents de travail:

- Le [document de référence](https://tinyl.io/416C) du projet et une [présentation préliminaire](https://tinyl.io/416B) fin 2020, qui décrit aussi la gouvernance, le budget, etc...
- Un [inventaire des contenus prévus](https://tinyl.io/416D) et une [structuration provisoire](https://docs.google.com/document/d/1zrt_5cG8SqDLChbuk8RKY7_g19FWP82DsX3UBbuGE_E/edit) qui servent de base au travail en cours.

### Comment participer au projet lui-même ?

Bienvenue ! Il suffit de [nous contacter](./7_Contact.html), effectivement notre équipe grandit régulièrement et c'est tant mieux car la tâche est immense.

Participer officiellement à ce projet est aussi une belle reconnaissance pour ces collègues qui se sont investi·e·s à fond depuis des mois pour que cet enseignement réusisse : c'est un levier pour savoir et partager ces contributions.

---
title: Pour qui et pour quoi ?
description: Présentation de l'offre et du public
hide:
- navigation
---

L'introduction de l'enseignement de l'informatique au lycée va permettre à une génération de maîtriser et participer au développement du numérique, pas uniquement le consommer. Le principal enjeu est alors la formation des enseignantes et des enseignants. Cet espace contribue à y aider.

## Pour qui ?

 Pour vous :)

### À qui s'adressent ces ressources ?
 
À tou·te·s les professeur·e·s d’informatique francophones qui veulent ou doivent se former et préparer, puis évaluer leurs enseignements.

Plus précisément tou·te·s les professeur·e·s d’informatique du niveau ``secondaire´´ francophones (voir ci-dessous, pour la terminologie).

En France, cela correspond aux collègues des lycées généraux et techniques enseignant 
 - les _sciences numériques et technologie_ [SNT](https://fr.wikipedia.org/wiki/Sciences_num%C3%A9riques_et_technologie), en [seconde](https://www.education.gouv.fr/bo/19/Special1/MENE1901641A.htm), qui permettent à tou·te·s de s'initier aux fondements du numérique et
 - l'enseignement de spécialité _numérique et sciences informatiques_ NS en [1ère](https://www.education.gouv.fr/bo/19/Special1/MENE1901633A.htm) et [Terminale](https://www.education.gouv.fr/bo/19/Special8/MENE1921247A.htm).

#### et aussi …

Au delà des enseignant·e·s et futur enseignant·e·s, ces ressources s'adressent:

- aux _parents_ et tout acteur ou actrice de l'éducation qui veut suivre et accompagner cette partie inédite des études que suivent les élèves désormais,
- aux _citoyen·ne·s_ qui n'ont pas eu la chance de profiter de ces enseignements et en on besoin au quotidien (par exemple parce que participant à un projet informatique),
- aux _cadres_ de l'éducation qui ont besoin d'avoir une vision précise de ces enseignements.

### Qui a vocation à enseigner l'informatique ?

On identifie trois grandes catégories de collègues ou collègues en devenir:

- Les _étudiant·e·s_ universitaires en informatique (ou élèves ingénieur·e·s) qui feront le choix de devenir enseignant : illes ont un bon niveau informatique et ont surtout besoin d'acquérir des compétences en [pédagogie](https://fr.wikipedia.org/wiki/P%C3%A9dagogie) et en [didactique de l'informatique](https://fr.wikipedia.org/wiki/Didactique).
- Les _collègues_ enseignant d'autres disciplines, souvent scientifiques (mais pas exclusivement), qui veulent varier de discipline : illes sont au fait de la pédagogie, doivent adapter la didactique de leur discipline à celle de l'informatique et doivent acquérir les bases des sciences et techniques informatiques, puis se former à cette discipline.
- Les _professionel·le·s_ de l'informatique, souvent très pointu·e·s dans un sous-domaine de l'informatique, doivent souvent élargir leurs compétences à d'autres sous-domaine, et comme les étudiant·e·s ont besoin d'acquérir des compétences en [pédagogie](https://fr.wikipedia.org/wiki/P%C3%A9dagogie) et en [didactique de l'informatique](https://fr.wikipedia.org/wiki/Didactique).

Bien entendu, celà n'est pas exclusif.

## Pour quoi ?

Pour obtenir deux classes de compétences : 

- (i) un _niveau en informatique théorique et pratique_ suffisant pour enseigner et prendre de la hauteur par rapport à cet enseignement
- (ii) une _capacité à enseigner ces savoirs, savoir-faire et savoir-être_ par rapport à l'informatique et au numérique.

### Que nous offre ces ressources ?

1. Une [formation disciplinaire](https://mooc-nsi-snt.gitlab.io/portail/3_Les_Fondamentaux.html) d'initiation, de formation à la programmation, et de formation aux fondamentaux de l'informatique.
2. Une [formation didactique](https://mooc-nsi-snt.gitlab.io/portail/4_Apprendre_A_Enseigner.html) pour apprendre, ensemble à enseigner, 
    - qui inclut aussi des ressources pour la [préparation au CAPES](https://www.societe-informatique-de-france.fr/capes-informatique).
3. Un [espace de partage et d'entraide](https://mooc-nsi-snt.gitlab.io/portail/5_Forum.html) pour être accompagner pendant la formation, la préparation et l'évaluation des enseignements

avec une invitation à [faire communauté](https://mooc-nsi-snt.gitlab.io/portail/6_Communaute.html) et bien entendu la [possibilité de contribuer](https://mooc-nsi-snt.gitlab.io/portail/7_Contribuer.html) à ces ressources partagées.

### Comment s'utilisent ces ressources ?

- En _auto-formation_ vous pouvez suivre les deux MOOCs qui seront disponibles à la rentrée 2021 et vous former individuellement ou en petite équipe d'éntraide, puis aller sur les forums et participer aux rencontres en ligne pour consolider et valider vos acquis.

- De _manière hybride_ ces ressources sont utilisées au sein de formation continue ou initiales, sous forme de travail personnel en ligne préalable, suivi de ``classes´´ présentielles. Ce sera par exemple le cas du Master MEEF-Info de l'[INSPÉ de l'académie de Nice](https://www.reseau-inspe.fr/annuaires/inspe-france/inspe-academie-nice).

## Vos principales questions !

### Ces formations sont elles payantes, chronophages, réutilisables ? 

Coûteuses en argent non, en temps oui. 

- Nous construisons ici des biens communs, librement utilisables et réutilisables 
   - Ils sont sous licence [CC-BY](https://creativecommons.org/licenses/by/4.0/deed.fr) ou [CC-BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr) selon le choix des autrices ou auteurs, et [CeCiLL-C](https://cecill.info/licences/Licence_CeCILL-C_V1-fr.html) pour les composants logiciels.

Rien n'est gratuit, mais ces ressources ont été financées par notre argent public, et aussi beaucoup de temps et d'énergie de collègues enseignant·e·s et enseignant·e·s-chercheur·es, au delà de leur missions et temps de travail, pour en faire un bien commun non-commercial, de façon à être accessible à toutes et tous, au delà des frontières aussi.

Mais _oui_ c'est un véritable investissement en temps, qui correspond à une formation universitaire de _deux ans à temps plein_ (on parle de 400 heures de travail cumulé, pour une personne qui débuterait sur tout les aspects). En pratique il faut étaler le suivi de ces formations sur une année voir deux ans, et prévoir quelques heures par mois pour avancer.

Ils sont donc pleinement réutilisables en totalité ou en partie, avec seule obligation de les citer, et une forte incitation à nous informer de leur réutilisation: cela permettra de vous aider et cela nous permettra de montrer leur usage.

### Quand tout cela sera-t-il disponible ?

Progressivement.

Le [forum](5_Forum.html), la [communauté d'apprentissage](6_Communaute.html), ce portail avec la possibilité de [contact](7_Contact.html) sont déjà disponibles.

Au niveau de la [formation aux fondamentaux](3_Les_Fondamentaux.html) les modules préliminaires sont ouverts et permettent d'acquérir les prérequis pour suivre cette formation qui va ouvrir au niveau de la rentrée 2021.

Au niveau de l'[apprentissage à enseigner](4_Apprendre_A_Enseigner.html) nous allons construire le contenu ensemble à partir de la rentrée 2021 : par un partage de pratiques, du travail en équipe sur des contenus de sciences de l'éducation, etc.

### Quelles sont les terminologies des classes du secondaire au sein du monde francophone ?

Nous ciblons les cours d’informatique du niveau ``secondaire´´ francophones au sens suivant:

| France  | Belgique  | Suisse | Québec | Allemagne| USA  |
| ---    |  ---     | ---    | ---    | ---       | --- |
| _(Lycée)_ | _(Secondaire)_ | _(Secondaire II)_ | _(Secondaire)_ | _(Oberstufe)_| _(High School)_ |
| 2nd    | 4ème     |  12ème  | IV     | 10ème     | K10 |
| 1ère   | 5ème Poésie |  13ème   | V DES    | 11ème     | K11 |
| Terminale | 6ème Réthorique |  14ème   | Préuniversitaire     | 12ème     | K12 |

selon les correspondances que l'on le lit [ici](https://fr.wikipedia.org/wiki/Comparaison_entre_le_syst%C3%A8me_d%27%C3%A9ducation_belge_et_d%27autres_syst%C3%A8mes_%C3%A9ducatifs) ou [là](https://sissevres.org/fr/content/tableau-d%C3%A9quivalence-de-niveau). La ``2nd´´ correspond à la 10ème année d'étude depuis l'apprentissage de la lecture.

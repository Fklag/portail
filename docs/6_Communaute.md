---
title: La Communauté
description: Agir ensemble au sein de la communauté d'apprentissage de l'informatique
hide:
- navigation
---

Bienvenue aussi dans la communauté d’apprentissage de l’informatique, au service des personnes qui apprennent et enseignent ces sujets pour aider chacune et chacun à maîtriser l’informatique et le numérique. On y aide les enseignant·e·s du primaire et du secondaire à enseigner l’informatique. Le site finit de se co-construire avec des collègues enseignant·e·s ou enseignant·e·s chercheur·e·s

https://cai.community

Participer à la communauté permet d'aller plus loin que les objectifs initiaux de formation et de préparationd es cours proposés ici, 

## Vos Questions !

### Comment s'inscrire sur la plateforme et l'utiliser ?

En trois clics et quelques informations minimales (Nom, Prénom, Email, Choix d'un mot de passe).

![Vue du bandeau de la plateforme CAI](./assets/banner-cai.png)

On explique aussi [comment ça marche](https://cai.community/accueil/comment-ca-marche) en quelques mots et images.

Si il y a le moindre souci [contactez-nous](./7_Contact.md).

### Peut-on en savoir plus sur la démarche ?

Avec plaisir :)

Ce projet européen dit [CAI](https://cai.community/accueil/qui-sommes-nous/partenaires/details) dans le cadre du [programme Erasmus+](https://ec.europa.eu/programmes/erasmus-plus/node_fr) contribue à développer et faire partager des outils pédagogiques et technologiques pour soutenir l’émergence d’une communauté d’enseignant·e·s en informatique.

Pour en savoir plus:

- [Description du projet](https://cai.community/accueil/qui-sommes-nous/partenaires/details) : la description du projet et sa [fiche de présentation](https://cai.community/accueil/qui-sommes-nous/partenaires).

- [Enjeux dans la création d'une communauté d'enseignants engagés dans l'apprentissage de l'informatique](https://hal.inria.fr/hal-02426274) : un papier scientifique, accessible à tou·te·s de positionnement qui décrit les enjeux et la démarche proposée.

- [Une para-plateforme pour faire communauté autour de l’enseignement de l’informatique](https://hal.inria.fr/hal-02994175v3/) : un rapport qui détaille la démarche au niveau de la plateforme, avec une analyse des besoins à couvrir et des solutions proposées.


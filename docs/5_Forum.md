---
title: Le Forum
description: Partager et s'entraîder sur le forum
hide:
- navigation
---

Le forum est l'endroit où nous partageons, discutons et aussi nous rassemblons pour co-créer des contenus et des ressources.

https://mooc-forums.inria.fr/moocnsi/

Le but principal est de faciliter la recherche d’information : le découpage en catégories / sous-catégories reprenant les items du programme permettra aux utilisateurs actuels et futurs de retrouver rapidement les informations sur un thème donné.

Au-delà de la communauté des enseignants actuels de NSI, ce forum sera aussi celui de la formation en ligne NSI, dont les futurs candidats aux CAPES, en lien avec la Communauté d’Apprentissage de l’Informatique francophone.

Ce forum propose des fonctionnalités avancées (par exemple possibilité d’écrire du code en Markdown au sein d’un message, moteur de recherche performant, possibilité de clôturer un sujet en mettant en avance sa solution…) que nous vous laissons le plaisir de découvrir.

## Vos Questions !

### Comment s'inscrire sur le forum ?

En trois clics et quelques informations minimales (Nom, Prénom, Email, Choix d'un mot de passe).

![Vue du bandeau du forum](./assets/banner-forum.png)

Si il y a le moindre souci [contactez-nous](./7_Contact.md).

#### Venir sur le forum via la plateforme FUN

__Attention !__ Si vous êtes inscrits sur la [plateforme FUN](https://www.fun-mooc.fr) car vous suivez des formations en ligne, alors vous allez être connectés directement sur ce forum, mais si vous créé un compte directement aussi, il y aura un doublon.

### Qui anime ce forum ?

Ce sont les collègues de l'[Association des enseignantes et enseignants d'informatique de France](http://aeif.fr) avec toute l'équipe que nous formons et le [Learning-Lab Inria](https://learninglab.inria.fr)  (Institut national de recherche en sciences et technologies du numérique), soutenus dans leur intention par l’Inspection Générale, offrent à la très active communauté gravitant autour de l’enseignement NSI (professeur·e·s des lycées, cadre de l’éducation nationale et collègues de l’enseignement supérieur et de la recherche en soutien).

### Quelle complémentarité avec la liste mail NSI ?

C'est grâce à liste de discussion nationale pour les Enseignants de la Spécialité Numérique & Sciences Informatiques de 1ère et Terminale, initiée et portée par Rodrigo Schwencke, que s'est formée une vraie communauté et, nous (les personnes qui co-animons le forum et les ressources proposées sur ce portail) restons abonnées à cette liste pour faire le lien.

Nous sommes passé·e·s à un mécanisme de forum, pour juste mieux structurer les échanges, pérenniser les ressources partagées etc. mais nous continuons de bien travailler en collaboration, mutualiser les outils, etc.

### Quel outil a été utilisé et pourquoi ?

Le forum est basé sur le logiciel libre [Discourse](https://www.discourse.org), porté par le [Learning-Lab Inria](https://learninglab.inria.fr), qui le déploie.

### A-t-on une évaluation de son usage ?

Oui, fin avril 2021, Il y avaut plus de 880 utilisateurs enregistrés, avec une engagement quotidien des utilisateurs, qq diaines par jour, et près de 850 posts, plus de détails [ici](https://cai.community/breve/en-direct-du-forum-nsi).

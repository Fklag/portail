FROM squidfunk/mkdocs-material

RUN apk --update --upgrade add gcc musl-dev jpeg-dev zlib-dev libffi-dev cairo-dev pango-dev gdk-pixbuf-dev
RUN pip install mkdocs-pdf-export-plugin mkdocs-macros-plugin

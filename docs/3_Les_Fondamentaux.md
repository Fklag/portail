---
title: Les Fondamentaux
description: Les Fondamentaux du numérique et des sciences informatiques
hide:
- navigation
---

## La formation : Apprendre les fondamentaux nécessaires pour ensuite enseigner au niveau du secondaire supérieur.

Ce cours est un MOOC du [Learning Lab Inria](https://learninglab.inria.fr/) qui sera diffusé sur https://www.fun-mooc.fr pendant plus de six mois à partir de l'automne 2021 pour se former au rythme de la personne apprenante, une attestation (gratuite) sera délivrée à son issue.


Cette page est encore en construction ... patience :) Tout ce qui est drafté ici reste à valider.

![](https://medsci-sites.inria.fr/cai/wp-content/uploads/sites/3/2020/04/Under-construction.png)

### Objectifs pédagogiques

L'objectif est de permettre à des enseignant·e·s ou en passe de l'être de se doter eux-mêmes des compétences en informatique permettant d'enseigner cela au niveau secondaire et de prendre aussi de la hauteur et du recul par rapport à la matière enseignée.

### Description du contenu

Voir https://tinyl.io/42Y9 et https://tinyl.io/42YA

L'espace de création de contenu : https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux

### Format de la formation

Sous forme de xMOOC (c'est à dire des vidéos de cours fractionnées en sections d'une 10zaines de minutes disponibles aussi sous formes textuelles, avec des exercices d'auto-évaluation de type quiz et autres, et des activités utilisant des outils numériques divers) nous offrons un parcours qui couvre l'ensemble du programme d'informatique enseigné au lycée en spécialité, y compris les notions en amont pour partager les savoirs et faire acquérir les savoir faire attendus. 

### Prérequis pour l'apprenant·e

Avoir les connaissances de base en informatique et être déjà initié à la programmation python, tel que explicité ci-dessous où on donne les liens vers les formations disponibles pour acquérir ces compétences.

### Compétences acquises et évaluation

Les compétences actives sont celles exprimées dans les objectifs pédagogiques.

L'évaluation de ces compétences se fera et en auto-évaluation sur la plateforme en ligne et par des activités cross-évaluées oar les pairs.

## Vos questions!

#### Qui participe à cette formation ?

Au sein de notre équipe nous avons :

* Anthony Jutton, Enseignant principal
* Alain Tchana, Enseignant
* Anne Josiane Kouam Djuigne, Enseignante
* Antoine Tran-Tan, Enseignant
* Charlotte Nachtegael, Enseignant
* Geoffrey Vaquette, Enseignant
* Gilles Geeraerts, Enseignant
* Isabelle Collignon, Ingénieure pédagogique
* Jean-Marc Vincent, Enseignant
* Marie-Hélène Comte, Ingénieure pédagogique
* Mihaela Sighireanu, Enseignante
* Philippe Marquet, Enseignant
* Sébastien Hoarau, Enseignant
* Tara Yahiya, Enseignante
* Thierry Massart, Enseignant
* Vania Marangozova-Martin, Enseignante


### Quelles sont les formations préliminaires à cette formation "lourde" ?

Il faut connaître les concepts de base et s’initier à la programmation. En bref on invite à [s'initier à l'enseignement en Sciences Numériques et Technologie (SNT)](https://www.fun-mooc.fr/fr/cours/sinitier-a-lenseignement-en-sciences-numeriques-et-technologie) et s'initier à la programmation Python. Il est aussi recommander d'approfondir sa culture scientifique du numérique.

#### Découvrir les concepts de base en informatique

- En allant https://classcode.fr/snt => _section S_ vous aurez les premières bases, le vocabulaire, les idées introductives, un moyen aussi de vérifier que cela est fait pour vous ;)

Note : There is also an [English version of these resources](https://pixees.fr/classcode-v2/classcode-informatics-and-digital-creation-online-free-open-course/) if of any help, including digital culture skills acquisition.

#### S’initier à la programmation Python :

Nous proposons trois solutions possibles, selon qu'on enseigne au niveau élémentaire de découverte de la programmation ou à un niveau plus avancé :

- Introduction minimale pour SNT : 
    - https://classcode.fr/snt => _section T_ sélectionne les bases pour commencer à programmer.
- Formation minimale pour NSI : 
    - [Apprendre à coder avec Python](https://www.fun-mooc.fr/courses/course-v1:ulb+44013+session04/about)
- Formation avancée pour NSI : 
    - [Python 3 : des fondamentaux aux concepts avancés du langage](https://www.fun-mooc.fr/courses/course-v1:UCA+107001+session02/about)

Bien entendu ce n'est pas exclusif, toute bonne formation à la programmation Python sera parfaite.

#### Approfondir sa culture scientifique et technique du numérique

On propose à cette fin deux autres sections de la formation [SNT](https://www.fun-mooc.fr/fr/cours/sinitier-a-lenseignement-en-sciences-numeriques-et-technologie)

- https://classcode.fr/snt => _section S+_ permet d'aborder des grands sujets du numériques à la fois techniques et sociétaux pour que les choses fassent du sens au delà de la formation disciplinaire à l'informatique.

- https://classcode.fr/snt => _section N_ correspond aux thématiques de l'enseignement des _sciences numériques et technologie_ [SNT](https://fr.wikipedia.org/wiki/Sciences_num%C3%A9riques_et_technologie), et permet de mieux connaître ces sujets.

On découvrira aussi avec bénéfice des éléments de l'histoire de l’informatique que nous proposons ici sous forme de deux ressources réutilisables avec les élèves: 

- https://project.inria.fr/classcode/profiter-de-classcode-en-postcast sous forme de podcats qui permettent de décourvriri des femmes et des hommes qui ont fait l'histoire de l'informatique

- http://sparticipatives.gforge.inria.fr/film qui présente ces contenus sous forme d'un livret et d'une web série.

Il faudra aussi disposer de ressources sur les métiers et secteurs industriels du numérique, c'est en cours.

### Quelles formations complémentaires sont indiquées ?

#### Intelligence artificielle

- https://classcode.fr/iai propose une formation citoyenne gratuite et attestée, 
    - qui correspond à la formation en IA qui se fait en France au niveau de l'enseignement des sciences en dernière année du secondaire.
    - et est proposée dans le cadre d’une perspective [« d’Université Citoyenne et Populaire en Sciences et Culture du Numérique »](https://hal.inria.fr/hal-02145480) où chacune et chacun de la chercheuse au politique en passant par l’ingénieure ou l’étudiant venons avec nos questionnements, nos savoirs et savoir-faire à partager.
- https://www.elementsofai.fr est une formation technique à l'IA, ``de niveau NSI´´ pour les enseignant·e·s ou les élèves qui voudraient aller plus loin sur ces sujets, grâce à la formation NSI.

On peut avoir une discussion sur ces formation [ici](https://www.lemonde.fr/blog/binaire/2021/01/12/formation-a-lintelligence-artificielle-la-bande-annonce/)

#### Autres compléments

- Informatique durable et équitable : un MOOC est en préparation dans le cadre de Class´Code "https : //classcode.fr/greenIT"

- Pour qui n'a jamais programmé, un cours d'initiation à Scratch, tel que pratiqué au collège et en primaire est disponible : https://project.inria.fr/classcode/initiation-a-scratch-en-autonomie 

- La robotique pédagogique est aussi un puissant le levier et [Le robot Thymio comme outil de découverte des sciences du numérique](https://www.fun-mooc.fr/courses/course-v1:inria+41017+session01/about) offre une formation de choix sur ces sujets.


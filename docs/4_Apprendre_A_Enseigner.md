---
title: Apprendre à enseigner
description: Apprendre à enseigner le Numérique et les Sciences Informatiques
hide:
- navigation
---

## La formation : Apprendre à enseigner le Numérique et les Sciences Informatiques

Ce cours est un MOOC du [Learning Lab Inria](https://learninglab.inria.fr/) qui sera diffusé sur https://www.fun-mooc.fr pendant plus de six mois à partir de l'automne 2021 pour se former en communauté de manière participatiove, une attestation (gratuite) sera délivrée à son issue.

Cette page est encore en construction ... patience :) Tout ce qui est drafté ici reste à valider.

![](https://medsci-sites.inria.fr/cai/wp-content/uploads/sites/3/2020/04/Under-construction.png)

### Objectifs pédagogiques

L'objectif est de permettre à des enseignant·e·s ou en passe de l'être d'apprendre à enseigner l'informatique au niveau du secondaire, donc de :

- Connaître les attendus en matière de compétences des élèves : savoir, savoir-faire et savoir-être,
    - à travers les programmes établis.
- Se familiariser avec les outils logiciels et organisationnels spécifiques de cet enseignement,
    - pouvoir manipuler une plateforme d'apprentissage de la programmation pour une classe,
    - utiliser des outils numériques d'apprentissage de concepts et de pratiques de l'informatique,
    - pouvoir organiser du travail en pédagogie inversée (invitation à découvrir le sujet, puis validation et approfondissement en cours)
    - savoir guider des progressions d'élèves en apprentissage semi-autonome,
    - etc.
- Se mettre en situation d'enseignement : 
    - pouvoir préparer des cours d'informatique théoriques et pratiques,
    - organiser ces cours au sein d'une progression, 
    - mettre en action l'enseignement dans la classe de la mise en activité à l'accompagnement des élèves, 
    - évaluation des acquis et
    - auto-évaluation et amélioration du cours.
- Apprendre par la pratique à créer les ressources dont on a besoin pour ces cours,
    - identifier et évaluer les supports disponibles (ouvrages, fiches, etc...),
    - adapter ces contenus à la situation pédagogique de la classe et à la démarche d'enseignement choisie,
    - thésauriser et reviser les contenus pour un partage collégial et une utilisation ultérieure.

### Description du contenu

Voir https://tinyl.io/42XY et https://tinyl.io/42XW

L'espace de création de contenu : https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique

_Remarques_:

- Une section de ce MOOC sera dédié à la préparation au CAPES.
- Un travail collégial dans le cadre de ce cours sera la création d'un manuel NSI collégial, sous licence libre.

### Format de la formation

Sous forme de cMOOC (c'est à dire un MOOC dit ``communautaire´´ où nous apprenons en créant les contenus ensemble de manière accompganée dont on a besoin) nous offrons un parcours à triple entrée

1. par l’exemple, basé sur des échanges et réalisations en groupe et des témoignages : on prend quelques activités typiques et regarde ensemble toute la démarche 
2. par les thématiques du programme : on co-construct des ressources au fil de l’année pour préparer les cours et évaluer leur réalisation, et apprendre à créer d'autres ressources
3. par une réflexion et un travail sur des thèmes transversaux (ex: travail en projet de groupes d'élèves, apprentissage en autonomie de la programmation, enjeux d'égalité des genres, information sur les débouchés de cet enseignement, pédagogie différenciée pour des élèves à particularité) 

### Prérequis pour l'apprenant·e

Avoir suivi et validé la [formation aux fondamentaux(./3_Les_Fondamentaux.html).

### Compétences acquises et évaluation

Les compétences actives sont celles exprimées dans les objectifs pédagogiques.

L'évaluation de ces compétences se fera de manière formelle par l'évaluation de productions numériques par les pairs et en auto-évaluation par rapport à sa pratique.

_Note :_ Aux personnes en formation qui ne sont pas en situation d'enseignement, il sera proposé des "simulations" pour tester les pratiques sur des publics tests.

## Vos questions!

#### Qui participe à cette formation ?

Au sein de notre équipe nous avons :

* Sébastien Hoarau, Enseignant principal
* Asli Grimaud, Enseignante
* David Roche, Enseignant
* Isabelle Collignon, Ingénieure pédagogique
* Marie-Hélène Comte, Ingénieure pédagogique
* Maxime-Simon Fourny, Enseignant
* Olivier Goletti, Chercheur en didactique

### Où sont les ressources partagées ?

Toutes nos ressources sont accessibles ici : https://gitlab.com/mooc-nsi-snt

Ces ressources sont faites de manière participative : bienvenue à tou·te·s les profesionnel·le·s de l'éducation qui souhaitent contribuer.

### Comment rejoindre le partage de ressources concrètement ?

- Commencer par:
    - S'incrire sur gitlab https://gitlab.com/users/sign_up ou
    - Se connecter https://gitlab.com/users/sign_in
- Demander à rejoindre le groupe https://gitlab.com/groups/mooc-nsi-snt 
    - en cliquant sur "Request Access" ou "Demander l'accès" en haut à gauche du titre,
    - en mettant quelques mots dans votre message pour dire qui vous êtes et quelle est votre motivation.

et voilà le tour est joué.

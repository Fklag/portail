---
title: Contact
description: Contactez-nous
hide:
- navigation
---

Rechercher une ressource ? Proposer une ressource ? Demander de l'aide sur une ressource ? Solliciter une aide en direct ? Poser une question ? Être mis·e en relation avec un·e collègue ? Contribuer au projet ?

Ce “bureau d’accueil” est ouvert à toutes et tous : enseignant·e·s et autres professionnel·le·s de l’éducation, curieux de science, et citoyen·ne·s.

Nous avons aussi un “salon en ligne” si vous avez besoin (un système de visio-réunion); pour l’utiliser, contactez-nous ici.

| Mail  | Twitter  | Facebook | Tel |
| ---   |  ---     | ---      | --- |
| [![contact-mail](./assets/contact-mail.png)](mailto:cai-contact@inria.fr?subject=Ma%20question%20à%20propos%20de%20la%20Communauté%20d'Apprentissage%20de%20l'Informatique&body=%20Ma%20question%20:%20%0A%0A%20Qui%20je%20suis%20%20: "cai-contact@inria.fr") | [![contact-twitter](./assets/contact-twitter.png)](https://twitter.com/intent/tweet?source=webclient&text=À+propos+de+https://www.cai.community+@CaiCommunity " ") | [![contact-facebook](./assets/contact-facebook.png)](https://www.facebook.com/Cai-community-111060377299220 " ") | [![contact-tel](./assets/contact-tel.png)](# "Au téléphone aussi ;) +33 4 92 38 76 88") |

Vous pouvez aussi vous exprimer plus précisément [via un formulaire](https://cai.community/contact/#frm_form_2_container).

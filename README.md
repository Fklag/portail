# Portail Moocs NSI

Ce dépôt contient la configuration et les templates du portail des moocs NSI.

## Organisation des fichiers

Les fichiers markdown sont compilés depuis le dossier `/docs`. 
Les moocs 0, 1 et 2 sont inclus via des sous-modules git.

## Déploiement local

Afin de contribuer et visualiser vos modifications, vous pouvez déployer le site localement 
sur votre ordinateur.

### Avec pip / conda

#### Installation

**Prérequis :**
- Installer Miniconda : https://docs.conda.io/en/latest/miniconda.html
- (Optionnel) Créer un environement `conda` dédié : 
  ```shell
  conda create --name mkdocs && source activate mkdocs
  ```
- Installer les dépendances :
  ```shell
  pip install mkdocs-material mkdocs-pdf-export-plugin mkdocs-jupyter mkdocs-macros-plugin
  ```

#### Visualisation en local

1. Cloner le dépôt, se placer dans le projet à la racine puis

```shell
mkdocs serve
```

Après quelques secondes, le site devrait être visible ici : http://localhost:8000

### Avec Docker

#### Installation

**Prérequis :**
- Installer Docker https://docs.docker.com/engine/install/

#### Visualisation en local

```shell
docker build -t mkdocs . && docker run --rm -it -p 8000:8000 -v ${PWD}:/docs mkdocs
```

Après quelques secondes, le site devrait être visible ici : http://localhost:8000
